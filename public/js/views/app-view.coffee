define [
  "beaver-console"
  "views/navbar-view"
  "jade!templates/app-view-tpl"
],(
  jconsole
  NavbarView
  AppViewTpl
)->

  sLogHeader = "[views/app-view]"
  jconsole.group "#{sLogHeader}"

  AppView = Backbone.View.extend {

    el : 'body'
    template : AppViewTpl

    initialize : ()->
      @cid = 'app'
      @jNavbarView = new NavbarView()
      @render()

    render: ()->
      jconsole.log "#{sLogHeader}.render()"

      @$el
       .empty()
       .append(@jNavbarView.$el)
       .append(@template())

      return this
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return AppView
