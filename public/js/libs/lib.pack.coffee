define [
  'beaver-console'
  'jquery-plugins'
  'underscore.string'
  'backbone.localStorage'
  'bootstrap-hover-dropdown'
], (jconsole) ->
  jconsole.info "lib-pack"
  _.mixin _.str.exports()
  return