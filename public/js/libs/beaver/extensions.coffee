define [], () ->

  # Add ECMA262-5 method binding if not supported natively
  if (!('bind' in Function.prototype))
    Function.prototype.bind = (owner)->
      that = this
      if (arguments.length<=1)
        return ->
          return that.apply(owner, arguments)
      else
        args= Array.prototype.slice.call(arguments, 1)
        return ->
          return that.apply(owner, if arguments.length==0 then args else args.concat(Array.prototype.slice.call(arguments)))

  # !2013.3.12 tuiteraz
  $.fn.hasAttr = (name) ->
    return !_.isUndefined this.attr(name)

  Math.RandomInteger = (n, m) ->
    m ||= 1
    max = if n > m then n else m
    min = if n == max then m else n
    d = max - min + 1
    return Math.floor(Math.random() * d + min)


  $.fn.listHandlers = (events) ->
    this.each (i) ->
#      jconsole.enable_log "$.listHandlers"
#      jconsole.group this
      elem = this
      #      dEvents = $(this).data('events')
      dEvents = $._data $(this)[0],'events'
      if !dEvents then return
      $.each dEvents, (name, handler) ->
        s1 = if events == '*' then '.+' else events.replace(',','|').replace(/^on/i,'')
        r = new RegExp("^#{s1}$",'i')
        if r.test(name)
          $.each handler, (i,handler)->
  #            jconsole.log "[#{name}] : %s", handler
            console.info elem,"[#{name}] : ", handler
#      jconsole.group_end()

  # !2012.12.26 tuiteraz
  _.mixin {
    capitalize : (sVal) ->
      return sVal.charAt(0).toUpperCase() + sVal.substring(1).toLowerCase()
  }

  # +2013.6.28 tuiteraz
  Array.prototype.clone = ->
    return this.slice 0

  window.browser = {}
  window.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase())
  window.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase())
  window.browser.opera = /opera/.test(navigator.userAgent.toLowerCase())
  window.browser.msie = /msie/.test(navigator.userAgent.toLowerCase())

  #+2013.9.14 tuiteraz
  #*2013.10.7 tuiteraz: using $.browser
  window.is_ie= (iVersion=null) ->
    if browser.msie
      if iVersion
        bRes = (parseInt($.browser.version) <= iVersion)
      else
        bRes = true
    else
      bRes = false

    return bRes

  #+2013.10.2 tuiteraz
  #*2013.10.7 tuiteraz: using $.browser
  window.is_opera= (iVersion=null) ->
    if browser.opera
      if iVersion
        bRes = (parseInt($.browser.version) <= iVersion)
      else
        bRes = true
    else
      bRes = false

    return bRes

    