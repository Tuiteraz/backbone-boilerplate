define [
  "beaver-console"
  "views/app-view"
],(
  jconsole
  AppView
  jSearchQueryCtrl
)->

  initialize:()->
    sLogHeader = "[app].initialize()"
    jconsole.info "#{sLogHeader}"
    @jAppView    = new AppView()
#
#    Backbone.history.start()
